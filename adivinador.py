import random

def adivina(numero):
	intentos = 100
	randomnum = random.randint(1,100)
	
	
	while (intentos > 1):
		
		if(randomnum == numero):
			print("\nGANASTEEE\n")
			return
		else:
			intentos = intentos -1
			print(f"Seguí intentando, te quedan {intentos} intentos")
			
			while True:
				numero = int(input("Ingrese otro número: "))
				if numero >=0 and numero <=10:
					break
			
			
	print("PERDISTE :(, no tenés más intentos")
	return

while True:
	print("\n\n\n\n\n/////////////////////////\n")

	print("PARA ENCONTRAR EL NÚMERO RANDOM entre 1 y 100 \n (tenés 10 intentos)")
	print("´0´ para salir")
	print("\n//////////////////////////\n\n\n")
	
	while True:
		num = int(input("Ingrese un número: "))
		if num >=0 and num <=10:
			break
			
	if num == 0:
		exit()
	else:
		adivina(num)
	

